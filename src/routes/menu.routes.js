const express = require("express");
const router = express.Router();
const Menu = require("../models/menu.models");
const {
  autorizerNewMenu,
  adminLogInAuthorizer,
} = require("../middlewares/basic-auth.middleware.js");

/**
 * @swagger
 * /menus/new:
 *  post:
 *      summary: to create new menu usea an admin credentials like username batman, password batman
 *      tags: [Menus]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Menu'
 *      responses:
 *          200:
 *              description: The menu user was created
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: The menu was created
 *          405:
 *              description: The menu user was not created, duplicated menu name
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: The menu user was not created
 *          401:
 *              description: admin credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: admin credentials not verified
 */
router.post("/new", adminLogInAuthorizer, autorizerNewMenu, (req, res) => {
  res.json(Menu.createNewMenu(req.body));
});

/**
 * @swagger
 * /menus/{name}:
 *  delete:
 *      summary: to delete menu from Menu list use an admin credentials like username batman, password batman
 *      tags: [Menus]
 *      parameters:
 *          -   in: path
 *              name: name
 *              schema:
 *                  type: string
 *              required: true
 *              description: The menu name
 *      responses:
 *          200:
 *              description: Menu deleted / Menu name does not exist
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: Menu deleted / Menu name does not exist
 *          401:
 *              description: admin credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: admin credentials not verified
 */

router.delete("/:name", adminLogInAuthorizer, (req, res) => {
  res.json(Menu.deleteMenu(req.params));
});

/**
 * @swagger
 * /menus/editMenu:
 *  put:
 *      summary: Update the Menu use an admin credentials like username batman, password batman
 *      tags: [Menus]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/editMenu'
 *      responses:
 *          200:
 *              description: The menu was updated / the new name already exist
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: The menu was updated / name Muzzarella already exist
 *          401:
 *              description: admin credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: admin credentials not verified
 */
router.put("/editMenu", adminLogInAuthorizer, (req, res) => {
  res.json(Menu.editMenu(req.body));
});

// router.put("/:", adminLogInAuthorizer, (req, res) => {
//   MenuName = req.body.name;
//   description = req.body.description;
//   price = req.body.price;

//   menu_body = {
//     name: MenuName,
//     description: description,
//     price: price,
//   };
//   Menu.createNewMenu(menu_body);
//   res.json("The new menu was created");
// });

/**
 * @swagger
 * /menus/all:
 *  get:
 *      summary: Return all the menu
 *      tags: [Menus]
 *      security: []
 *      responses:
 *          200:
 *              description: The menu list
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Menu'
 *          401:
 *              description: Bad conection
 */

router.get("/all", (req, res) => {
  console.log(Menu.findAll());
  res.json(Menu.findAll());
});

/**
 *  @swagger
 *  tags:
 *      name: Menus
 *      description: Menu section
 *
 * components:
 *  schemas:
 *      Menu:
 *          type: object
 *          required:
 *              - name
 *              - description
 *              - price
 *          properties:
 *              name:
 *                  type: string
 *                  description: The name of the food. Unic
 *              description:
 *                  type: string
 *                  description: The description of the food
 *              price:
 *                  type: integer
 *                  description: The price of the food
 *          example:
 *              name: Muzzarella
 *              description: salsa de tomates, muzzarella y aceitunas
 *              price: 800
 *      editMenu:
 *          type: object
 *          required:
 *              - name
 *          properties:
 *              name:
 *                  type: string
 *                  description: The name of the food
 *              newName:
 *                  type: string
 *                  description: The new name of the food. Unic
 *              newPrice:
 *                  type: string
 *                  description: The price name of the food.
 *              newDescription:
 *                  type: string
 *                  description: The new description of the food.
 *          example:
 *              name: Muzzarella
 *              newName: Muzza
 *              newDescription: Mucho queso
 *              newPrice: 20
 */

module.exports = router;
