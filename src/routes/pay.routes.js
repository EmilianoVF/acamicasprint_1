const express = require("express");
const router = express.Router();
const Pay = require("../models/pay.models");
const {
  adminLogInAuthorizer,
} = require("../middlewares/basic-auth.middleware.js");

/**
 * @swagger
 * /pays:
 *  get:
 *      summary: Return the list of pay method
 *      tags: [Pays]
 *      responses:
 *          200:
 *              description: The list of pay method
 */
router.get("/", adminLogInAuthorizer, (req, res) => {
  res.json(Pay.findAll());
});

/**
 * @swagger
 * /pays/newPayMethod:
 *  post:
 *      summary: add new pay method. admin credentials
 *      tags: [Pays]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - payMethod
 *                      properties:
 *                          payMethod:
 *                            type: string
 *                            description: The new pay method
 *                      example:
 *                        payMethod: paga Iron-Man
 *      responses:
 *          200:
 *              description: list of pay method
 *          401:
 *              description: credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: credentials not verified
 */
router.post("/newPayMethod", adminLogInAuthorizer, (req, res) => {
  res.json(Pay.newPayMethod(req.body));
});

/**
 * @swagger
 * /pays/{payMethod}:
 *  delete:
 *      summary: delete pay method. admin credentials
 *      tags: [Pays]
 *      parameters:
 *          -   in: path
 *              name: payMethod
 *              schema:
 *                  type: string
 *              required: true
 *              description: pay method to delete
 *      responses:
 *          200:
 *              description: pay method list
 *          401:
 *              description: admin credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: admin credentials not verified
 */
router.delete("/:payMethod", adminLogInAuthorizer, (req, res) => {
  res.json(Pay.deletePayMethod(req.params));
});

/**
 * @swagger
 * /pays/editPayMethod:
 *  put:
 *      summary: edit pay method. admin credentials
 *      tags: [Pays]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - payMethod
 *                          - newPayMethod
 *                      properties:
 *                        payMethod:
 *                          type: string
 *                          description: pay method
 *                        newPayMethod:
 *                          type: strinf
 *                          description: new pay method
 *                      example:
 *                        payMethod: paga Batman
 *                        newPayMethod: paga Batman, obvio
 *      responses:
 *          200:
 *              description: list of pay method
 *          401:
 *              description: credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: credentials not verified
 */
router.put("/editPayMethod", adminLogInAuthorizer, (req, res) => {
  res.json(Pay.editPayMethod(req.body));
});

module.exports = router;
