const express = require("express");
const router = express.Router();
const Orders = require("../models/order.models");
const basicAuth = require("express-basic-auth");

const {
  logInAuthorizer,
  adminLogInAuthorizer,
  checkPayMethod,
} = require("../middlewares/basic-auth.middleware.js");

// newOrder
/**
 * @swagger
 * /orders/newOrder:
 *  post:
 *      summary: Create new order
 *      tags: [Orders]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/order'
 *      responses:
 *          200:
 *              description: The user current orders and next pay amount
 *              content:
 *                  object:
 *                      $ref: '#/components/schemas/orderResponse'
 *          401:
 *              description: credentials not verified
 */
router.post(
  "/newOrder",
  basicAuth({ authorizer: logInAuthorizer }),
  (req, res) => {
    res.json(Orders.newOrder(req.body));
  }
);
// userConfirmOrder;
/**
 * @swagger
 * /orders/userConfirmOrder:
 *  put:
 *      summary: user confirm the order by id
 *      tags: [Orders]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - username
 *                          - id
 *                      properties:
 *                        username:
 *                          type: string
 *                          description: the username name
 *                        id:
 *                          type: integer
 *                          description: id of the menu to confirm
 *                      example:
 *                        username: robin
 *                        id: 1
 *      responses:
 *          200:
 *              description: OK, the status change. not OK, the status did not change
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: OK, the status change. not OK, the status did not change
 *          401:
 *              description: credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: credentials not verified
 */
router.put(
  "/userConfirmOrder",
  basicAuth({ authorizer: logInAuthorizer }),
  (req, res) => {
    res.json(Orders.userConfirmOrder(req.body));
  }
);

// adminChangeStatus;
/**
 * @swagger
 * /orders/adminChangeStatus:
 *  put:
 *      summary: admin change status. admin credentials
 *      tags: [Orders]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - username
 *                          - id
 *                          - newStatus
 *                      properties:
 *                        username:
 *                          type: string
 *                          description: the username name
 *                        id:
 *                          type: integer
 *                          description: id of the menu to change status
 *                        newStatus:
 *                          type: string
 *                          description: the newStatus can be Entregado, En preparacion o Enviado
 *                      example:
 *                        username: robin
 *                        id: 1
 *                        newStatus: Entregado
 *      responses:
 *          200:
 *              description: OK, the status change. not OK, the status did not change
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: OK, the status change. not OK, the status did not change
 *          401:
 *              description: credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: credentials not verified
 */
router.put("/adminChangeStatus", adminLogInAuthorizer, (req, res) => {
  res.json(Orders.adminChangeStatus(req.body));
});

// adminCancelOrder;
/**
 * @swagger
 * /orders/adminCancelOrder:
 *  put:
 *      summary: admin cancel order. admin credentials
 *      tags: [Orders]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - username
 *                          - id
 *                      properties:
 *                        username:
 *                          type: string
 *                          description: the username name
 *                        id:
 *                          type: integer
 *                          description: id of the menu to cancel
 *                      example:
 *                        username: robin
 *                        id: 1
 *      responses:
 *          200:
 *              description: OK, the order cancel. not OK, the order not cancel
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: OK, the order cancel. not OK, the order not cancel
 *          401:
 *              description: credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: credentials not verified
 */
router.put("/adminCancelOrder", adminLogInAuthorizer, (req, res) => {
  res.json(Orders.adminCancelOrder(req.body));
});

// userCancelOrder;
/**
 * @swagger
 * /orders/userCancelOrder:
 *  put:
 *      summary: user cancel order
 *      tags: [Orders]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - username
 *                          - id
 *                      properties:
 *                        username:
 *                          type: string
 *                          description: the username name
 *                        id:
 *                          type: integer
 *                          description: id of the menu to cancel
 *                      example:
 *                        username: robin
 *                        id: 1
 *      responses:
 *          200:
 *              description: OK, the order cancel. not OK, the order not cancel
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: OK, the order cancel. not OK, the order not cancel
 *          401:
 *              description: credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: credentials not verified
 */
router.put(
  "/userCancelOrder",
  basicAuth({ authorizer: logInAuthorizer }),
  (req, res) => {
    res.json(Orders.userCancelOrder(req.body));
  }
);

// userChangeOrder;
/**
 * @swagger
 * /orders/userChangeOrder:
 *  put:
 *      summary: user change order
 *      tags: [Orders]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - username
 *                          - id
 *                      properties:
 *                        username:
 *                          type: string
 *                          description: the username name
 *                        id:
 *                          type: integer
 *                          description: id of the menu to cancel
 *                        newQty:
 *                          type: integer
 *                          description: new quantity
 *                        address:
 *                          type: string
 *                          description: address to send the order if not default address will be use
 *                        payMethod:
 *                          type: string
 *                          description: pay method if not default pay method will be use
 *                      example:
 *                        username: robin
 *                        id: 1
 *                        newQty: 3
 *                        address: casa de Gatubela
 *                        payMethod: paga Gatubela
 *      responses:
 *          200:
 *              description: OK, order chance. not OK, the order did not change
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: OK, order chance. not OK, the order did not change
 *          401:
 *              description: credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: credentials not verified
 */
router.put(
  "/userChangeOrder",
  basicAuth({ authorizer: logInAuthorizer }),
  checkPayMethod,
  (req, res) => {
    res.json(Orders.userChangeOrder(req.body));
  }
);

// allCurrentOrders;
/**
 * @swagger
 * /orders/allCurrent:
 *  get:
 *      summary: All current orders. admin credentials
 *      tags: [Orders]
 *      responses:
 *          200:
 *              description: The list of all current orders
 *          401:
 *              description: credentials not verified
 */
router.get("/allCurrent", adminLogInAuthorizer, (req, res) => {
  res.json(Orders.allCurrentOrders());
});

// allHistoricOrders;
/**
 * @swagger
 * /orders/allHistoric:
 *  get:
 *      summary: All current historic orders. admin credentials
 *      tags: [Orders]
 *      responses:
 *          200:
 *              description: The list of all current historic orders
 *          401:
 *              description: credentials not verified
 */
router.get("/allHistoric", adminLogInAuthorizer, (req, res) => {
  res.json(Orders.allHistoricOrders());
});

// CurrentOrders/:username
/**
 * @swagger
 * /orders/CurrentOrders/{username}:
 *  get:
 *      summary: Get current orders by username
 *      tags: [Orders]
 *      parameters:
 *          -   in: path
 *              name: username
 *              schema:
 *                  type: string
 *              required: true
 *              description: The user name
 *      responses:
 *          200:
 *              description: The list of current orders
 *          401:
 *              description: credentials not verified
 */
router.get(
  "/CurrentOrders/:username",
  basicAuth({ authorizer: logInAuthorizer }),
  (req, res) => {
    res.json(Orders.CurrentOrders(req.params));
  }
);

// HistoricOrders/:username
/**
 * @swagger
 * /orders/HistoricOrders/{username}:
 *  get:
 *      summary: Get historic orders by username
 *      tags: [Orders]
 *      parameters:
 *          -   in: path
 *              name: username
 *              schema:
 *                  type: string
 *              required: true
 *              description: The user name
 *      responses:
 *          200:
 *              description: The list of historic orders
 *          401:
 *              description: credentials not verified
 */
router.get(
  "/HistoricOrders/:username",
  basicAuth({ authorizer: logInAuthorizer }),
  (req, res) => {
    res.json(Orders.HistoricOrders(req.params));
  }
);

/**
 * @swagger
 * tags:
 *  name: Orders
 *  description: Orders section
 *
 * components:
 *  schemas:
 *      order:
 *          type: object
 *          required:
 *              - username
 *              - foodName
 *              - qty
 *          properties:
 *              username:
 *                  type: string
 *                  description: the user name. Unic
 *              foodName:
 *                  type: string
 *                  description: the menu name
 *              qty:
 *                  type: integer
 *                  description: the quantity
 *              address:
 *                  type: string
 *                  description: the address to send the order
 *              payMethod:
 *                  type: string
 *                  description: the payMethod
 *          example:
 *              username: robin
 *              foodName: Muzzarella
 *              qty: 1
 *              address: casa de Batman
 *              payMethod: paga Batman
 */

module.exports = router;
