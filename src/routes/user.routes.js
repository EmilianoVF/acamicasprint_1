const express = require("express");
const router = express.Router();
const User = require("../models/user.model");
const basicAuth = require("express-basic-auth");
const {
  adminLogInAuthorizer,
  autorizerNewUser,
  logInAuthorizer,
  checkPayMethod,
} = require("../middlewares/basic-auth.middleware.js");

/**
 * @swagger
 * /users/newUser:
 *  post:
 *      summary: Create new user
 *      tags: [Users]
 *      security: []
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/newUser-Admin'
 *      responses:
 *          200:
 *              description: The user was created
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: The user was created
 *          402:
 *              description: The user was not created
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: The user was not created
 */

router.post("/newUser", autorizerNewUser, checkPayMethod, (req, res) => {
  res.json(User.createNewUser(req.body, false));
});

/**
 * @swagger
 * /users/newAdmin:
 *  post:
 *      summary: to create new admin usea an admin credentials like username batman, password batman
 *      tags: [Users]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/newUser-Admin'
 *      responses:
 *          200:
 *              description: The admin user user was created
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: The admin user superman was created
 *          402:
 *              description: The user name already exist
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: The admin user was not created
 *          401:
 *              description: admin credentials not verified
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: admin credentials not verified
 */
router.post("/newAdmin", adminLogInAuthorizer, autorizerNewUser, (req, res) => {
  res.json(User.createNewUser(req.body, true));
});

/**
 * @swagger
 * /users/login:
 *  post:
 *      summary: Log in to the page
 *      tags: [Users]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/login'
 *      responses:
 *          200:
 *              description: User Created
 *              content:
 *                  text/plain:
 *                    schema:
 *                      type: string
 *                      example: Login correct
 *          401:
 *              description: username or email are not correct
 *              content:
 *                  text/plain:
 *                    schema:
 *                      type: string
 *                      example: username or email are not correct
 */
router.post(
  "/login",
  basicAuth({ authorizer: logInAuthorizer }),
  (req, res) => {
    res.json("Login correct");
  }
);

/**
 * @swagger
 * /users:
 *  get:
 *      summary: Return the list of users
 *      tags: [Users]
 *      security: []
 *      responses:
 *          200:
 *              description: The list of users
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/newUser-Admin'
 *          401:
 *              description: username and password are required
 */
router.get("/", (req, res) => {
  console.log(req.auth);
  res.json(User.findAll());
});

/**
 * @swagger
 * tags:
 *  name: Users
 *  description: User section
 *
 * components:
 *  schemas:
 *      login:
 *          type: object
 *          required:
 *              - username
 *              - password
 *          properties:
 *              username:
 *                  type: string
 *                  description: the user name. Unic
 *              password:
 *                  type: string
 *                  description: the user pass.
 *          example:
 *              username: robin
 *              password: robin
 *      newUser-Admin:
 *          type: object
 *          required:
 *              - username
 *              - password
 *              - email
 *              - address
 *              - payMethod
 *          properties:
 *              username:
 *                  type: string
 *                  description: the user name. Unic
 *              password:
 *                  type: string
 *                  description: the user pass
 *              email:
 *                  type: string
 *                  description: the user email. Unic
 *              address:
 *                  type: string
 *                  description: default user address.
 *              payMethod:
 *                  type: string
 *                  description: default user payMethod.
 *          example:
 *              username: superman
 *              password: 123456
 *              email: superman@superman.com
 *              address: Metropolis 999
 *              payMethod: paga Batman
 *
 */

module.exports = router;
