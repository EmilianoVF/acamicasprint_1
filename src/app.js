const express = require("express");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");

const userRoutes = require("./routes/user.routes");
const menuRoutes = require("./routes/menu.routes");
const orderRoutes = require("./routes/order.routes");
const payRoutes = require("./routes/pay.routes");
const swaggerOptions = require("./utils/swaggerOptions");

const app = express();

app.use(express.json());

// Documentacion swagger
const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use("/users", userRoutes);
app.use("/menus", menuRoutes);
app.use("/orders", orderRoutes);
app.use("/pays", payRoutes);
app.listen(3000, () => {
  console.log("server listing on port 3000");
});
