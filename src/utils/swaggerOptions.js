const swaggerOptions = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Ejemplo uso swagger",
      version: "1.0.0",
      description: "Como usar swagger en nodejs",
    },
    servers: [
      {
        url: "http://localhost:3000",
        description: "Local server",
      },
      {
        url: "http://www.development.com:3000",
        description: "Dev server",
      },
    ],
    components: {
      securitySchemes: {
        basicAuth: {
          type: "http",
          scheme: "basic",
        },
      },
    },
    security: [
      {
        basicAuth: [],
      },
    ],
  },
  apis: ["./routes/*.js"],
};

module.exports = swaggerOptions;
