const basicAuth = require("express-basic-auth");
const User = require("../models/user.model");
const Menu = require("../models/menu.models");
const Pay = require("../models/pay.models");

function logInAuthorizer(username, password) {
  const users = User.findAll().filter((u) => u.username === username);
  if (users.length <= 0) return false;

  const userMatches = basicAuth.safeCompare(username, users[0].username);
  const passwordMatches = basicAuth.safeCompare(password, users[0].password);

  return userMatches & passwordMatches;
}

function adminLogInAuthorizer(req, res, next) {
  try {
    username_password = new Buffer.from(
      req.headers.authorization.split(" ")[1],
      "base64"
    ).toString();
  } catch {
    return res.status(401).json("admin credentials not verified");
  }
  username = username_password.split(":")[0];
  password = username_password.split(":")[1];

  const users = User.findAll().filter((u) => u.username === username);
  if (users.length <= 0) return false;
  const passwordMatches = users[0].password === password;
  const adminMatches = users[0].isAdmin === true;
  if (passwordMatches & adminMatches) {
    next();
  } else {
    res.status(401).json("admin credentials not verified");
  }
}

function autorizerNewUser(req, res, next) {
  const { username, email } = req.body;
  const users = User.findAll().filter((u) => u.username === username);
  const emails = User.findAll().filter((u) => u.email === email);
  if (users.length > 0 || emails.length > 0) {
    return res.status(402).json("The user was not created");
  } else {
    next();
  }
}

function autorizerNewMenu(req, res, next) {
  new_name = req.body.name;
  const menus = Menu.findAll().filter((u) => u.name === new_name);
  if (menus.length > 0) {
    return res.status(405).json("The new menu was not created");
  } else {
    next();
  }
}

function checkPayMethod(req, res, next) {
  const { payMethod } = req.body;
  method = Pay.findAll().findIndex((u) => u === payMethod);
  if (method !== -1) {
    next();
  } else {
    res.status(401).json("invalid pay method");
  }
}

exports.logInAuthorizer = logInAuthorizer;
exports.autorizerNewUser = autorizerNewUser;
exports.adminLogInAuthorizer = adminLogInAuthorizer;
exports.autorizerNewMenu = autorizerNewMenu;
exports.checkPayMethod = checkPayMethod;
