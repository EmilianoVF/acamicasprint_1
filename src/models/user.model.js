const users = [
  {
    username: "batman",
    password: "batman",
    email: "batman@batman.com",
    address: "baticueva 999",
    payMethod: "VISA",
    isAdmin: true,
  },
  {
    username: "robin",
    password: "robin",
    email: "robin@robin.com",
    address: "casa de batman",
    payMethod: "Paga Batman",
    isAdmin: false,
  },
];

const findAll = () => {
  return users;
};

const createNewUser = (body, isAdmin) => {
  const { username, password, email, address, payMethod } = body;

  form_body = {
    username: username,
    password: password,
    email: email,
    address: address,
    payMethod: payMethod,
    isAdmin: isAdmin,
  };

  users.push(form_body);
  return `the new user user ${username} was created`;
};

module.exports = { findAll, createNewUser };
