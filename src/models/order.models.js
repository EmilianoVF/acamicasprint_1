const Menu = require("./menu.models");
const Users = require("./user.model");

orders = [
  {
    username: "emiliano",
    history: [],
    current: [],
    total_pay: 0,
    next_pay: 0,
  },
  {
    username: "julieta",
    history: [],
    current: [],
    total_pay: 0,
    next_pay: 0,
  },
];

const firstOrder = (newUsername) => {
  const user = orders.filter((u) => u.username === newUsername);
  if (user.length === 0) {
    user_file = {
      username: newUsername,
      address: "",
      history: [],
      current: [],
      total_pay: 0,
      next_pay: 0,
    };
    orders.push(user_file);
  }
};

const pay = (username) => {
  const user = orders.filter((u) => u.username === username);
  total_pay = 0;
  if (user[0].history.length > 0) {
    user[0].history.forEach((order) => {
      if (
        order.status !== "Cancel by Admin" &&
        order.status !== "Cancel by User"
      ) {
        total_pay += order.price * order.qty;
      }
    });
  }
  next_pay = 0;
  if (user[0].current.length > 0) {
    user[0].current.forEach((order) => (next_pay += order.price * order.qty));
  }
  return [total_pay, next_pay];
};

const newOrder = (body) => {
  const { username, foodName, qty } = body;
  firstOrder(username);
  const user = orders.filter((u) => u.username === username);
  const food = Menu.findAll().filter((u) => u.name === foodName);
  const userInfo = Users.findAll().filter((u) => u.username === username);
  let address = undefined;
  let payMethod = undefined;
  if ("address" in body) {
    address = body.address;
  } else {
    address = userInfo[0].address;
  }

  if ("payMethod" in body) {
    payMethod = body.payMethod;
  } else {
    payMethod = userInfo[0].payMethod;
  }

  if (food.length > 0) {
    newOrderInfo = {};
    newOrderInfo.foodName = foodName;
    newOrderInfo.description = food[0].description;
    newOrderInfo.price = food[0].price;
    newOrderInfo.status = "Pendiente";
    newOrderInfo.qty = qty;
    newOrderInfo.payMethod = payMethod;
    newOrderInfo.address = address;
    newOrderInfo.id = user[0].current.length + 1;
    user[0].current.push(newOrderInfo);
    const totalNextPay = pay(username);
    user[0].total_pay = totalNextPay[0];
    user[0].next_pay = totalNextPay[1];
    return { current: user[0].current, next_pay: user[0].next_pay };
  } else {
    return "Username or Menu name not found";
  }
};

const userConfirmOrder = (body) => {
  const { username, id } = body;
  const user = orders.filter((u) => u.username === username);
  if (user.length > 0) {
    const index = user[0].current.findIndex((u) => u.id === id);
    if (index !== -1) {
      user[0].current[index].status = "Confirmado";
      console.log(user[0].current[index]);
      return "OK";
    } else {
      return "Not OK";
    }
  } else {
    return "Not OK";
  }
};

const adminChangeStatus = (body) => {
  const { username, id, newStatus } = body;
  if (["Entregado", "En preparacion", "Enviado"].includes(newStatus)) {
    const user = orders.filter((u) => u.username === username);
    if (user.length > 0) {
      const index = user[0].current.findIndex((u) => u.id === id);
      if (index !== -1) {
        user[0].current[index].status = newStatus;
        if (newStatus === "Entregado") {
          user[0].history.push(user[0].current[index]);
          user[0].current.splice(index, 1);
        }
        return "OK";
      }
    }
  } else {
    return "not OK";
  }
};

const adminCancelOrder = (body) => {
  const { username, id } = body;
  const user = orders.filter((u) => u.username === username);
  const index = user[0].current.findIndex((u) => u.id === id);
  if (index !== -1) {
    if (user[0].current[index].status !== "Entregado") {
      user[0].current[index].status = "Cancel by Admin";
      user[0].history.push(user[0].current[index]);
      user[0].current.splice(index, 1);
      const totalNextPay = pay(username);
      user[0].total_pay = totalNextPay[0];
      user[0].next_pay = totalNextPay[1];
      return "OK";
    }
  } else {
    return "not OK";
  }
};

const userCancelOrder = (body) => {
  const { username, id } = body;
  const user = orders.filter((u) => u.username === username);
  const index = user[0].current.findIndex((u) => u.id === id);
  if (index !== -1) {
    if (
      user[0].current[index].status === "Pendiente" ||
      user[0].current[index].status === "Confirmado"
    ) {
      user[0].current[index].status = "Cancel by User";
      user[0].history.push(user[0].current[index]);
      user[0].current.splice(index, 1);
      const totalNextPay = pay(username);
      user[0].total_pay = totalNextPay[0];
      user[0].next_pay = totalNextPay[1];
      return "OK";
    } else {
      return "not OK";
    }
  } else {
    console.log("Order can not be canceled");
    return "not OK";
  }
};

const userChangeOrder = (body) => {
  const { username, id, newQty, address, payMethod } = body;
  const user = orders.filter((u) => u.username === username);
  const index = user[0].current.findIndex((u) => u.id === id);

  if (index !== -1) {
    if (
      user[0].current[index].status === "Pendiente" ||
      user[0].current[index].status === "Confirmado"
    ) {
      if (newQty) user[0].current[index].qty = newQty;
      if (address) user[0].current[index].address = address;
      if (payMethod) user[0].current[index].payMethod = payMethod;
      const totalNextPay = pay(username);
      user[0].total_pay = totalNextPay[0];
      user[0].next_pay = totalNextPay[1];
      return "OK";
    }
  }
};

const allCurrentOrders = () => {
  allCurrent = [];
  orders.forEach((order) => {
    if (order.current.length > 0) {
      obj = {};
      obj.name = order.username;
      obj.current = order.current;
      allCurrent.push(obj);
    }
  });
  return allCurrent;
};

const allHistoricOrders = () => {
  allHistoric = [];
  orders.forEach((order) => {
    if (order.history.length > 0) {
      obj = {};
      obj.name = order.username;
      obj.history = order.history;
      allHistoric.push(obj);
    }
  });
  return allHistoric;
};

const CurrentOrders = (body) => {
  const { username } = body;
  const user = orders.filter((u) => u.username === username);
  if (user.length > 0 && user[0].current.length > 0) {
    return user[0].current;
  } else {
    return `${username} never order`;
  }
};

const HistoricOrders = (body) => {
  const { username } = body;
  const user = orders.filter((u) => u.username === username);
  if (user.length > 0 && user[0].history.length > 0) {
    return user[0].history;
  } else {
    return `${username} never order`;
  }
};

module.exports = {
  newOrder,
  adminCancelOrder,
  userCancelOrder,
  allCurrentOrders,
  allHistoricOrders,
  CurrentOrders,
  HistoricOrders,
  userConfirmOrder,
  adminChangeStatus,
  userChangeOrder,
};
