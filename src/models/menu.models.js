const { json } = require("express");

const menus = [
  {
    name: "Muzzarella",
    description: "salsa de tomates, muzzarella y aceitunas",
    price: 800,
  },
  {
    name: "Muzzarella2",
    description: "salsa de tomates, muzzarella y aceitunas",
    price: 800,
  },
];

const findAll = () => {
  return menus;
};

const createNewMenu = (body) => {
  const { name, description, price } = body;
  menu = {
    name: name,
    description: description,
    price: price,
  };
  menus.push(menu);
  return `The new menu ${name} was created`;
};

const deleteMenu = (body) => {
  const { name } = body;
  const index = menus.findIndex((u) => u.name === name);
  if (index !== -1) {
    menus.splice(index, 1);
    return `Menu ${name} deleted`;
  } else {
    return "Menu name does not exist";
  }
};

const editMenu = (body) => {
  const { name, newName, newPrice, newDescription } = body;
  const menu = menus.filter((u) => u.name === name);
  if (menu.length !== 0) {
    if (newName) {
      const nameMenu = menus.filter((u) => u.name === newName);
      if (nameMenu.length === 0) {
        menu[0].name = newName;
      } else {
        return `name ${newName} already exist`;
      }
    }

    if (newPrice) menu[0].price = newPrice;
    if (newDescription) menu[0].description = newDescription;
    return menu[0];
  } else {
    return "menu name does not exist";
  }
};

module.exports = { findAll, createNewMenu, deleteMenu, editMenu };
