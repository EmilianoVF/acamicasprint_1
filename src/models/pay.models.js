Payments = ["paga Batman", "paga Gatubela", "paga Superman", "paga Dios"];

const findAll = () => {
  return Payments;
};

const newPayMethod = (body) => {
  const { payMethod } = body;
  Payments.push(payMethod);
  return Payments;
};

const deletePayMethod = (body) => {
  const { payMethod } = body;
  index = Payments.findIndex((u) => u === payMethod);
  if (index !== -1) {
    Payments.splice(index, 1);
    return Payments;
  }
};

const editPayMethod = (body) => {
  const { payMethod, newPayMethod } = body;
  index = Payments.findIndex((u) => u === payMethod);
  console.log(index);
  if (index !== -1) {
    Payments[index] = newPayMethod;
    return Payments;
  }
};

module.exports = { findAll, newPayMethod, deletePayMethod, editPayMethod };
